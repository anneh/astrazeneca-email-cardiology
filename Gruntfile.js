module.exports = function (grunt) {

    // Project configuration
    grunt.initConfig({

        // Read package.json
        pkg: grunt.file.readJSON('package.json'),

        premailer: {
            simple: {
                options: {
                    verbose: true,
                    preserveStyles: false
                },
                files: {
                    'maandelijks.html': ['maandelijks-src.html']
                }
            },

            event1: {
                options: {
                    verbose: true,
                    preserveStyles: false
                },
                files: {
                    'event1.html': ['event1-src.html']
                }
            },

            event2: {
                options: {
                    verbose: true,
                    preserveStyles: false
                },
                files: {
                    'event2.html': ['event2-src.html']
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 9000,
                    livereload: 35729,
                    hostname: '0.0.0.0',
                    base: './'
                }
            }
        },

        // We use the watch task for SASS compiling and livereloading
        watch: {
            options: {
                interrupt: true,
                livereload: true
            },
            html: {
                files: ['maandelijks-src.html', 'event1-src.html', 'event2-src.html'],
                //tasks: ['includereplace'],
                tasks: ['premailer'],
                options: {
                    livereload: true
                }
            }
        }

    });

    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-premailer');
    grunt.loadNpmTasks('grunt-contrib-connect');

    // The build task, run in terminal: grunt build
    /*grunt.registerTask('build', [
        'clean:prepare',
        'copy:build',
        'jshint',
        'removelogging',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'usemin',
        'imagemin',
        'clean:build'
    ]);*/

    // The watch task for SASS and HTML compiling. Run in terminal: grunt watch, or just: grunt
    grunt.registerTask('default', ['connect:server', 'watch']);

};